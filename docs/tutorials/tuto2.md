# TAPAS Tutorial 2: Create your Processing Pipeline
In this tutorial we will learn some basic functions of [TAPAS](/tapas-doc/). We will learn how to use the wizard to create some processing pipelines. A pipeline is a series of small modules that will process an image from the input to the quantitative analysis. Please check you understand the basics of [TAPAS Input/Output](tuto1). 

## Selecting a module
The wizard is called **TAPAS MENU** and can be found in TAPAS in the list of plugins or above the toolbar. 

<iframe width="560" height="315" src="https://www.youtube.com/embed/CxmhaJqqNZk" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

![menu1](../imgs/menu1.png)

This window will list all the available modules from the file _tapas.txt_ located in the ImageJ folder. When a module is selected, the available parameters are displayed on the right. If default values exist for these parameters they will be displayed. A short description of the selected module is also available below the list of modules.

The **first** module should always be an **input module**, to load data from OMERO or a folder. This can be written as:

```java
    // input module, using default image as input
    process:input
```

In this tutorial we will start by **denoising** the image by using a 3D median filter. Select the module **filters**. This module has 3 parameters, the radius in XY, radius in Z and the name of the filter. A list of modules with description of parameters is available in [Tapas Description0.6.3.pdf](../uploads/TapasDescription0.6.3.pdf). You can change parameters if you want, the list of available parameters are: median, mean, tophat, open, close, min and max. Then just click on **create text** to create the corresponding text for this module, in our case it is simply :

```java
    process:filters
    radxy:2
    radz:1
    filter:median
```

![menu2](../imgs/menu2.png)

There is no specific order for parameters. Note also that only parameters with modified or required values are to be explicitly entered; parameters with default values, if they exist, do not need to be entered, they can be omitted if you do not modify them, a short version of **filters** is then:

```java
    // use the module filters with default values
    process:filters
```

If you modify the filter but not the default radii, the text is then:

```java
    process:filters
    filter:mean
```

If all parameters are modified they need to be entered:

```java
    process:filters
    filter:mean
    radxy:4
    radz:2
```

## Your first processing pipeline
We will  build a quite classical processing pipeline: 

  - input data
  - denoising with 3D median filter
  - thresholding 
  - labeling
  - quantitative measurements

The input module can be written as:

```java
    process:input
```

Then we use the module **filters** as seen above :

```java
    process:filters
    radxy:2
    radz:1
    filter:median
```

The next module is then **thresholding**, we start with the most basic thresholding, with a fixed value. By default, the _value_ is set to 128, of course adjust it to your image. The parameter _user_ will be explained in another tutorial, for now use the default value.

```java
    // parameters with default values can be omitted if they are not modified
    process:threshold
    value:128
```

The image is now binarized, where the values of the pixels are either 0 for background or 1 for objects, but we still need to detect individual objects, this is done by labeling the image (aka segmentation). Select the module **label**, it has 2 parameters _minVolume_ and _maxVolume_. These two parameters correspond to the minimum volume and maximum volume (in number of voxels) of the objects to be detected, if the objects are too small or too big they will be discarded from the segmentation. The value -1 for maximum volume means we do not fix any maximum volume, only too small objects will be discarded.

```java
    // parameters with default values can be omitted if they are not modified
    process:label
    minVolume:10
```

The image is now labeled, where the value of the pixel is the object id. The final module is **measurement**, it will perform geometric measurement of the objects present in the image. The **list** of available measurements are : volume, area, centroid, compactness, ellipsoid, DC (Distance to Center). Simply enter the name of the measurements separated by **,**. The measurements result will be saved as .csv file (that you can open with a spreadsheet software or within ImageJ). You will need to indicate the **directory** and the **file name** where to save this file.
By default, the name of the file will be _results.csv_ and the directory will be the home directory using the keyword _?home?_  

Possible keywords for folders are :

  - _?home?_ for the user home directory
  - _?ij?_ for the ImageJ directory
  - _?tmp?_ for the system temporary directory

These directories can be used with sub-directories such as : _?home?/MyResults_ or _?ij?/plugins/TAPAS_. 

The text for the **measurement** module will look like this :

```java
    // parameters with default values can be omitted if they are not modified
    process:measurement
    dir:?home?
    file:?image?-results.csv
    list:volume,centroid
```

By pressing each time the _create text_ button, you should obtain a complete processing pipeline text.

![menu3](../imgs/menu3.png)

Finally, you can copy and paste the text in a text file and save it on your computer. You can then use **TAPAS OMERO** or **TAPAS FILES** to run the pipeline on selected images. 

## Pushing back results
If you want to push back the results into OMERO or into the files folder, you can use the module **attach**. First, you need to indicate which file you want to push back, by specifying its **directory** (_dir_) and its **file name** (_file_). Then you want to indicate to which image this file should be attached to, you need to specify the _project_, _dataset_ and _image_ name of the image. By default, the module will attach the file to the current processed image.

The keyword _?project?_ corresponds to the project name of the currently processed image, similarly _?dataset?_ corresponds to the dataset name of the currently processed image and _?image?_ to the name of the currently processed image. The keywords can be coupled with static text, for example if all the results are saved in another dataset called the name of the original dataset followed by _-results_, then we simply indicate _?dataset?-results_. Note that in case you are using **TAPAS FILES**, the file will be copied into the folder corresponding to the specified dataset. 

We should obtain, in case we simply attach the results to the original image:

```java
    // we attach the results file to the current image
    // in case we are using FILES the file will be copied into the dataset folder.
    process:attach
    dir:?home?
    file:?image?-results.csv
```

## Putting all together

The final pipeline will hence look like this:

```java
    // input data
    process:input
    
    //reduce noise by filtering
    process:filters
    radxy:2
    radz:1
    filter:median

    // thresholding data
    process:threshold
    value:128

    // extracting the objects
    process:label
    minVolume:10

    // performing geometrical measurements
    // and saving results locally
    process:measurement
    dir:?home?
    file:?image?-results.csv
    list:volume,centroid

    // we finally attach the results file to the current image
    // in case we are using FILES the file will be copied into the dataset folder.
    process:attach
    dir:?home?
    file:?image?-results.csv
```
## Applying the pipeline to a set of images using OMERO

Once you have designed your pipeline you can apply it to a set of images. If your image data is stored on OMERO, you will need the **TAPAS OMERO** plugin. 

<iframe width="560" height="315" src="https://www.youtube.com/embed/E0iXD1UTMqk" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

With this plugin you can navigate through OMERO, select your projet, dataset and the images you want to process; if you did not select any images, it will ask you if you want to process all images. 

You will also have to select the previously saved pipeline to use. 

![tapas-omero1](../imgs/Tapas-Omero1.png)

After clicking **run processing**, TAPAS will initialise and process your images sequentially; note that the images may not be processed in order. The details of the processing, along with the time to process will be displayed in the _Log_ window. 

![tapas-omero2](../imgs/Tapas-Omero2.png)

## Applying the pipeline to a set of images with files saved locally

Once you have designed your pipeline you can apply it to a set of images. If your image data is stored locally, you will need the **TAPAS LOCAL** plugin. 

<iframe width="560" height="315" src="https://www.youtube.com/embed/S9znKem3hRs" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

With this plugin you can navigate through your folders, select a root to all your projects, select your projet, dataset and the images you want to process; if you did not select any images, it will ask you if you want to process all images. 

You will also have to select the previously saved pipeline to use. You can use the same pipeline to process data locally or stored on OMERO. 

![tapas-local](../imgs/Tapas-Local.png)

After clicking **run processing**, TAPAS will initialise and process your images sequentially; note that the images may not be processed in order. The details of the processing, along with the time to process will be displayed in the _Log_ window. 

![tapas-omero1](../imgs/Tapas-Omero2.png)
