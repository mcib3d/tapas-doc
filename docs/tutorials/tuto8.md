# TAPAS Tutorial 8: Layers analysis
In this tutorial we will learn some basic analysis functions of [TAPAS](/tapas-doc/). We will learn how to use the **EDT** and **EVF** modules to create continuous layers inside a structure. Then we will use the **EVF layers analysis** module to quantify the distribution of spots within the structure.Please check you understand the basics of [TAPAS Input/Output](tuto1).

## Computing the layers
The **EVF** (Eroded Volume Fraction) can be regarded as a _normalized_ **EDT** (Euclidean Distance Map), it was described in [this article](https://onlinelibrary.wiley.com/doi/full/10.1002/jcb.21823). The **[EDT](https://en.wikipedia.org/wiki/Distance_transform)** will compute for each pixel inside the structure of interest the **distance to the border** of the structure and output a map where the pixel value is actually the distance.

Here we assume we already have a binarised structure image with value _0_ for the background. First we need to **input** the binary image of the structure, compute the **EVF** and **save** it locally. The input data can either be a file on disk or an image in a OMERO database.

```java
    // We use here the raw data as the reference image and
    // we assume the binary image has the same name
    // as the reference image with -structure
    process:input
    name:?image?-structure

    // compute the evf
    // this module computes the edt map by default
    // we put yes to ask the module to compute the evf instead of the edt
    process:edt_evf
    evf:yes

    // and we save the evf image locally
    // here in the ImageJ/Fiji folder
    process:save
    dir:?ij?
    file:?image?-evf.tif
```
![evf-bin](../imgs/evf-bin.png)
![evf-evf](../imgs/evf-evf.png)

## Layers analysis
The values in the EVF are normalized values between _0_ and _1_ and, then can be used to compute layer distribution inside many structures regardless of their size. Furthermore, in case of a random distribution, the distribution within EVF layers should be equal, since, in EVF, all layers have equal volume. Note that using EDT to compute distribution within layers may lead to biased results, since volumes based on distance to border will not have equal volumes.

![evf-evf](../imgs/evf-evf.png)
![evf-edt](../imgs/evf-edt.png)

Compare the EVF on the left and the EDT on the right.

We will use the **evfLayers** module to compute distribution of spots within a fixed number of layers (of equal volume) within the EVF image. We need to define the number of layers and the path to the corresponding EVF image.
The input for the **evfLayers** analysis should be a binary or labelled image of the spots, the module will compute in each layer the volume occupied by the spots.

![evf-spots](../imgs/evf-spots.png)

```java
    // We use here the raw data as the reference image and
    // we assume the binary spots has the same name as the reference image with -spots
    // input corresponding spots
    process:input
    name:?image?-spots

    // layer analysis
    // path to the previously saved evf image
    // and where to save the results
    // and number of layers (100 by default)
    process:evfLayers
    dirEvf:?ij?
    fileEvf:?image?-evf.tif
    dir:?ij?
    file:?imagz?-layer
    nbLayers:100
```

The output of the **evfLayers** module will be first the plot and values for the input data, respectively as PNG and csv file. In this example with 100 layers, a perfect random distribution should be flat with a value of _0.01_ in each layer. We can then see in this example a trend toward low evf values, indicating that the spots are mostly localized near the border of the structure. 


![evf-plot](../imgs/evf-plot.png)

The module will also output with the suffix _all_ the plot for volumes of the different layers, and due to numerical approximation, the volumes of the layers are not exactly equal, this output can be hence used for further normalisation.  

![evf-plot-all](../imgs/evf-plot-all.png)

## Saving the results and cleaning temporary files

The results are saved as temporary files, we must _attach_ them to the original raw data. The module **attach** will _attach_ results file to the image within Omero or will copy the results in the same folder as the original image if data are on files. 

```java
    // attach files to data in omero or on files
    // first attach png and csv of the spots layer analysis
    process:attach
    dir:?ij?
    file:?image?-layer.csv
    process:attach
    dir:?ij?
    file:?image?-layer.png

    // then attach png and csv of the volume layer analysis
    process:attach
    dir:?ij?
    file:?image?-layer-all.csv
    process:attach
    dir:?ij?
    file:?image?-layer-all.png
```

Finally, we can delete the temporary files.

```java
    // delete evf
    process:delete
    dir:?ij?
    file:?image?-evf.tif

    // delete plots
    process:deleteList
    dir:?ij?
    list:?image?-layer.csv,?image?-layer.png,?image?-layer-all.csv,?image?-layer-all.png
```
