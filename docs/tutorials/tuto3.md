# TAPAS Tutorial 3: Segmentation
In this tutorial we will learn some basic functions of [TAPAS](/tapas-doc/). We will learn how to use the thresholding and segmentation modules. Please check you understand the basics of [TAPAS Input/Output](tuto1). 

In this tutorial we will use the **DotBlot** image from ImageJ, download it from _Open Sample_. We will first invert it to get bright objects over a dark background. 

![dot_blot-inv](../imgs/dot_blot-inv.png)

## Basic thresholding
Thresholding will simply create a binary image based on the pixel value and a threshold value, if the pixel value is lower than the threshold value, it will be considered as background and is displayed usually in black, if the pixel value is greater than the threshold value, it will be considered as object and will be displayed in white.

The module **threshold** will simply ask for the threshold value and will perform the thresholding, by default, the threshold value is 128 but this should be changed according to your image. The code is then :

```java
    // thresholding with a fixed value
    // default value is 128
    process:threshold
    value:128
```

In our example a value of 128 will not detect most of the dots.

![dot_blot-inv-th128](../imgs/dot_blot-inv-th128.png)

A value of 65 will detect more dots, but will introduce some noise.

![dot_blot-inv-th65](../imgs/dot_blot-inv-th65.png)

## Automatic thresholding
Automatic thresholding will compute an optimal threshold for your image according to its histogram. Many algorithms exist to try to determine the best threshold, they are detailed [here](https://imagej.net/Auto_Threshold). The most famous algorithms are : IsoData, Otsu, Yen or Triangle. 

The module **autoThreshold** implements some of these algorithms :

  - Isodata
  - Otsu
  - Intermodes
  - Yen
  - Triangle
  - Mean
  - Huang
  - IJ_Isodata

Simply enter the thresholding algorithm name in the parameter _method_. An option is also available to specify if your image has a _dark_ background, the value by default is _yes_, simply put _no_ for an image with a light background. 

```java
    // auto-thresholding
    // enter the method name 
    // and if the background is dark (yes by default)
    process:autoThreshold
    method:Otsu
```

In our example the Otsu method gives this result :

![dot_blot-inv-otsu](../imgs/dot_blot-inv-otsu.png)

and the Triangle method gives better results:

![dot_blot-inv-triangle](../imgs/dot_blot-inv-triangle.png)

## Iterative thresholding
The algorithm Iterative Thresholding is implemented in [here](https://imagejdocu.list.lu/plugin/segmentation/3d_spots_segmentation/start). 
Basically all thresholds will be tested and for each threshold if an object appear and has the right size, the object is kept. If an object appears at different thresholds, only the threshold leading to the object with the best shape is retained. 

The module **IT** will implement this algorithm, you need to specify the minimum volume of the objects with _minVolume_, the maximum volume with _maxVolume_. You can also specify a noise level, a minimum threshold value, _minThreshold_, to start the thresholdings. The default value for _minVolume_ is 100, for _maxVolume_ it is -1 (mo maximum limit) and the noise level default value, _minThreshold_, is 0. 

```java
    // Iterative thresholding
    // specify minimum and maximum volume
    // and noise level (default 0)
    process:IT 
    minVolume:10
```

In our example, if we set a minimum volume of 10 we get a lot of noise.

![dot_blot-inv-itmin10](../imgs/dot_blot-inv-itmin10.png)

A minimum value of 200 allows us to detect all the dots without any background noise objects. 

![dot_blot-inv-itmin200](../imgs/dot_blot-inv-itmin200.png)

## Hysteresis thresholding
The algorithm Hysteresis Thresholding is implemented in [here](https://imagejdocu.list.lu/plugin/segmentation/3d_spots_segmentation/start). 
Basically two thresholds are defined, a high and a low threshold. Objects are first thresholded with the low threshold, only objects containing a pixel with  a value higher than the high threshold value are retained, other objects are discarded. 

In our example we define the two thresholds 65 and 128 as explained in the above section about basic thresholding. We can build an image with three values, 0 (displayed in black) for pixels with value below the low threshold, 128 (displayed in gray) for pixels with value above low threshold but below high threshold, and 255 (displayed in white) for pixels with value above high threshold.

![dot_blot-inv.png_multi](../imgs/dot_blot-inv.png_multi.png)

Only gray objects containing white pixel will be retained, other will be discarded. We then obtain the following binary image containing only retained objects. 

![hyst_dot_blot-inv65-128-Bin](../imgs/hyst_dot_blot-inv65-128-bin.png)

The module **hysteresis** will implement the hysteresis thresholding. The low and high thresholds are specified in _minValue_ and _maxValue_ (there are no default values). 

```java
    // hysteresis thresholding
    // you need to specify low and high threshold values
    process:hysteresis
    minValue:65
    maxValue:128
```

This module can also directly label the image, _i.e._ detect and segment the different objects in the image.

![hyst_dot_blot-inv65-128](../imgs/hyst_dot_blot-inv65-128.png)

You need simply to set the parameter _labeling_ to _yes_ (the value is _no_ by default).

```java
    // hysteresis thresholding
    // you need to specify low and high threshold values
    // you can label the image directly by setting the parameter labeling to yes
    process:hysteresis
    minValue:65
    maxValue:128
    labeling:yes
```


