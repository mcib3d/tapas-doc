# TAPAS Tutorial 6: Colocalisation
In this tutorial we will learn some basic analysis functions of [TAPAS](/tapas-doc/). We will learn how to use the **multiColoc** module to compute **co-localisation** between two populations.  Please check you understand the basics of [TAPAS Input/Output](tuto1).

## Input data
Here we will need two labelled images as inputs, the **multiColoc** module will then find the pairs of objects co-localising and compute the volume of their intersection. First we will need to get one image as input and save it as a temporary file.

![coloc-a](../imgs/coloc-a.png)

```java
    // We use here the raw data as the reference image and
    // we assume the first labelled image has the same name 
    // as the reference image with -spots2
    process:input
    name:?image?-spots2

    // we then save this file locally
    // here in the ImageJ/Fiji folder
    process:save
    dir:?ij?
    file:?image?-coloc2.tif
```

We can then get the other image as input.

![coloc-b](../imgs/coloc-b.png)

```java
    // We use here the raw data as the reference image and
    // we assume the first labelled image has the same name 
    // as the reference image with -spots1
    process:input
    name:?image?-spots1
```

## Computing co-localisation
We then use the **multiColoc** module to compute co-localisation between the two spots populations. The module will require to specify the path to the other labelled image, in our case the image _spots2_ that we saved locally, and the path to save the results. 

```java
    // Multi-colocalisation between two images
    // we need the path to the other label image
    // here image spots1
    // results will be saved locally
    process:multiColoc
    dirLabel:?ij?
    fileLabel:?image?-spots2.tif
    dir:?ij?
    file:?image?-coloc.csv
```

![coloc-resultstable](../imgs/coloc-resultstable.png)

The first column **label** refers to the objects in the first image, called _A_ here _spots1_ with their label (id) value. The second column **O1** will refer to the label of a co-localised object in the second image, called _B_ here _spots2_ The third column **V1** is the intersection volume between the two objects co-localising, in number of pixels. The fourth column **P1** is the ratio of the intersection volume over the  volume of object in _A_ giving an idea of the importance of the co-localisation. If the object in _A_ co-localises with a second object, the column **O2**, **V2** and **P2** will give information about the co-localisation between object in _A_ and a second object in image _B_. 

In our example, the object with value 295 in image _A_ will co-localise with object with value 1124 in image _B_ with an intersection volume of only 3 pixels. The object 295 in image _A_ does not co-localise with a second object in image _B_ 

## Saving the results and cleaning temporary files
The results are saved as temporary files, we must _attach_ them to the original raw data. The module **attach** will _attach_ results file to the image within Omero or will copy the results in the same folder as the original image if data are on files. 

```java
    // attach results file to data in omero or on files
    process:attach
    dir:?ij?
    file:?image?-coloc.csv
```

Finally we can delete the temporary file. 

```java
    // delete temporary spot2 file
    process:delete
    dir:?ij?
    file:?image?-spots2.tif
```





