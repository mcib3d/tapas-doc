These files are for the TAPAS framework for automation version 0.6.4, https://mcib3d.frama.io/tapas-doc/. Please copy the file tapas.txt and tapas-documentation.txt to the ImageJ or Fiji folder.

Please install the OMERO insight-ij-client if you want to connect to OMERO, https://www.openmicroscopy.org/omero/downloads/. The version number should match the version number of your server. Please do not install OMERO-insight from the Fiji update site.

Please install the bioformats package form the Fiji update site or https://www.openmicroscopy.org/bio-formats/downloads/ . 
