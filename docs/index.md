## TAPAS : An integrated tool for batch processing
**TAPAS** (Towards an Automated Processing and Analysis System) is a framework for processing and analysis workflow for multi-dimensional images. A workflow is a series of modules linked together to process an image, each module should perform one simple task.

The first idea is to design a framework where **users** can share the pipeline as simple text file. The second idea is to create a simple programming template so **developers** can create their own module.

Finally, the pipeline should be separated from the set of images to process, so a same pipeline can be used for different images. The results of the processing and the analysis should be stored along with the processed images.

Here we propose modules to process and analyze images stored in an OMERO database, but the system should work fine with files stored locally or on a server.

## Author
Main developer Thomas Boudier, with some help from F. Lafouresse, L. Whitehead and J.-F. Gilles.

## Features
Basically we will propose modules from the [3D ImageJ Suite](https://mcib3d.frama.io/3d-suite-imagej/), ImageJ/Fiji, along with other modules.

As for stable version 0.6.4 the following modules are available :

-   Input/Output to files (using BioFormats)
-   Input/Output to OMERO
-   2D/3D filtering
-   Auto-threshold, hysteresis and Iterative thresholding
-   2D/3D labeling
-   Objects measurements, signal quantification and numbering
-   Colocalisation and distances
-   Layers and interactions
-   Calling ImageJ macros and executables

## Installation
-   Install [ImageJ](https://imagej.nih.gov/ij/) or [Fiji](http://fiji.sc/)
-   Install [3D ImageJ Suite](https://mcib3d.frama.io/3d-suite-imagej/). It is also available as a update site in Fiji.
-   Install **Bioformats**, either by downloading [bioformats_package.jar](https://www.openmicroscopy.org/bio-formats/downloads/) or by enabling the update site in Fiji.
-   Install **Simple Omero Client** by downloading simple-omero-client-XXX.jar from [GReD](https://github.com/GReD-Clermont/simple-omero-client/releases/)
-   Install OMERO package by downloading [OMERO-insight-ij](https://www.openmicroscopy.org/omero/downloads/), unzip OMERO.insight-ij-xxx.zip into the plugins directory of ImageJ or Fiji. 
-   Download [tapas-0.8.zip](uploads/tapas-0.8.zip) and unzip it into the ImageJ or Fiji directory.

TAPAS was tested successfully with Simple-Omero-Client 5.16.0, OMERO.insigh-ij 5.8.3 and Bioformats 6.0.1.

<div>
<iframe width="560" height="315" src="https://www.youtube.com/embed/YPbFG9P7FpU" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
</div>

## Usage / Tutorials
-   If you want to process data stored on an Omero server, connect first to your server using **TAPAS Connect** inside the TAPAS menu.
-   **Create your pipeline** in a text editor, check available tapas modules and how to use it in the documentation provided.
-   Run *TAPAS OMERO* if you want to process data from Omero or *TAPAS FILES* for data on files.

A list of tutorials :

-   Basic **Input/Ouptput** functionalities [here](tutorials/tuto1).
-   Create your first **processing pipeline** [here](tutorials/tuto2).
-   **Segmentation** modules [here](tutorials/tuto3).
-   **Measurement** modules [here](tutorials/tuto4).
-   **Signal quantification** and **numbering** modules [here](tutorials/tuto5).
-   **Co-localisation** [here](tutorials/tuto6).
-   How to use the **analyzeParticles** module for segmentation and measurement [here](tutorials/tuto7).
-   How to quantify **layers distribution** [here](tutorials/tuto8).

The list of available modules is described in [Tapas Description0.6.4.pdf](uploads/TapasDescription0.6.4.pdf).

<iframe width="560" height="315" src="https://www.youtube.com/embed/x96dPqkexXs" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

## Citation
Gilles JF and Boudier T. TAPAS: Towards Automated Processing and Analysis of multi-dimensional bioimage data. F1000Research 2021, 9:1278 [doi](https://doi.org/10.12688/f1000research.26977.2) 

## License
Version 0.5 and later are under GPL distribution (see [license](https://choosealicense.com/licenses/gpl-3.0/)).
Source code is available on **Framagit** : [tapas-core](https://framagit.org/mcib3d/tapas-core), [tapas-plugins](https://framagit.org/mcib3d/tapas-plugins).

## Change log
-   23/11/2022 V0.7.0 : new Menu and documentation, new modules.
-   08/12/2021 V0.6.4 : new module Mask. Improved documentation for menu. Default dir for most plugins is ?ij?.
-   22/06/2020 V0.6.3 : new modules (CLIJFilters, EDT+EVF, localThickness, CloseLabels). Changed _?name?_ by _?image?_. Generic image processing, default is ImagePlus.Folder *attachments* for TAPAS-FILES.
-   19/09/2019 V0.51 : minor change, new module load attachment from Omero
-   29/08/2019 V0.5 : stable version. New modules (see list)
-   06/05/2019 V0.41beta : fix bug in input/output on Windows
-   05/03/2019 V0.4beta : channels and frames now start at 1, new binning option in Omero Load, module 3dfilters is renamed filters and has parameter _radxy_ (merging _radx_ and _rady_), new tutorials on measurements and quantification
-   01/02/2019 V0.3beta : first beta release
